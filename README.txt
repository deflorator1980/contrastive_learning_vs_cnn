predict_mnist                  - all methods

train_contrastive_short_224    - 94%, OOM (reduce epochs)

train_contrastive_short_56     - not bad
train_contrastive_short        
train_contrastive_two
train_contrastive_secont
train_contrastive_32x32x3

train_contrastive_folder_best  - 100% in 1/4 cases
train_contrastive_folder       - max 94%
train_contrastive_folder_no_es - 100% in 1/4 cases
train_cl_augm_my               - 100% in 1/4 cases
train_cl_augm_my_es            - max 94%

source:
    https://keras.io/examples/vision/supervised-contrastive-learning/
    https://towardsdatascience.com/understanding-contrastive-learning-d5b19fd96607